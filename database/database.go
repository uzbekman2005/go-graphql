package database

import (
	"context"
	"log"
	"time"

	"gitlab.com/uzbekman2005/go-graphql/graph/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Db struct {
	client *mongo.Client
}

func Connect() *Db {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	return &Db{
		client: client,
	}
}

func (db *Db) CreateTask(input *model.NewTodo) *model.Todo {
	collection := db.client.Database("todo").Collection("tasks")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	res, err := collection.InsertOne(ctx, input)
	if err != nil {
		log.Fatal(err)
	}
	result := &model.Todo{
		ID:          res.InsertedID.(primitive.ObjectID).Hex(),
		Title:       input.Title,
		Description: input.Description,
		Dedline:     input.Dedline,
		Done:        false,
	}

	// userCollection := db.client.Database("todo").Collection("users")
	// userRes := userCollection.FindOne(ctx, bson.M{"_id": input.UserID})

	// user := &model.User{}
	// err = userRes.Decode(&user)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// result.User = user
	return result
}

func (db *Db) FindTaskByID(ID string) *model.Todo {
	ObjectID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		log.Fatal(err)
	}
	collection := db.client.Database("todo").Collection("tasks")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	res := collection.FindOne(ctx, bson.M{"_id": ObjectID})
	task := model.Todo{}
	err = res.Decode(&task)
	if err != nil {
		log.Fatal(err)
		return &model.Todo{}
	}

	return &task
}

func (db *Db) All() []*model.Todo {
	collection := db.client.Database("todo").Collection("tasks")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cur, err := collection.Find(ctx, bson.D{})
	if err != nil {
		log.Fatal(err)
	}
	var tasks []*model.Todo
	for cur.Next(ctx) {
		var task *model.Todo
		err := cur.Decode(&task)
		if err != nil {
			log.Fatal(err)
		}
		tasks = append(tasks, task)
	}
	return tasks
}

func (db *Db) CreateUser(input *model.NewUser) *model.User {
	collection := db.client.Database("todo").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	res, err := collection.InsertOne(ctx, input)
	if err != nil {
		log.Fatal(err)
	}
	result := &model.User{
		ID:        res.InsertedID.(primitive.ObjectID).Hex(),
		FirstName: input.FirstName,
		Email:     input.Email,
		LastName:  input.LastName,
	}

	return result
}

func (db *Db) FindUserByID(ID string) *model.User {
	ObjectID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		log.Fatal(err)
	}
	collection := db.client.Database("todo").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	res := collection.FindOne(ctx, bson.M{"_id": ObjectID})
	user := model.User{}
	err = res.Decode(&user)
	if err != nil {
		log.Fatal(err)
		return &model.User{}
	}

	return &user
}

func (db *Db) AllUsers() []*model.User {
	collection := db.client.Database("todo").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cur, err := collection.Find(ctx, bson.D{})
	if err != nil {
		log.Fatal(err)
	}
	var users []*model.User
	for cur.Next(ctx) {
		var user *model.User
		err := cur.Decode(&user)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, user)
	}
	return users
}
